<?php
namespace App\Core;
//se App\Core\Database;
/**
 * Base Model
 */
class Model
{
        public function __construct() 
        {
            //parent::__construct(DB_TYPE, DB_HOST, DB_NAME, DB_USER, DB_PASS);
            //$this->db = new Database(DB_TYPE, DB_HOST, DB_NAME, DB_USER, DB_PASS);
        }

        protected static function getDb()
        {
            static $db = null;
            if ($db === null) 
            {
                $dsn = DB_TYPE.':host=' . DB_HOST . '; dbname=' . DB_NAME . ';charset=utf8';
                $db = new PDO($dsn, DB_USER, DB_PASS);

                // Throw an Exception when an error occurs
                $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            }

            return $db;

        }



} //END CLASS
?>
