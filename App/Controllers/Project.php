<?php
namespace App\Controllers;
use App\Core\Controller;
use App\Core\View;
/**
 *  Project
 */
class Project extends Controller
{

	public function __construct()
	{
		parent::__construct();

		//Setting global css file
		self::setStyles('app');
	}

	public function indexAction()
	{
			View::render('f1', 'project');
	}

    protected function before(){}

    protected function after(){}


} //END CLASS