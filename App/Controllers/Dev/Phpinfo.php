<?php
namespace App\Controllers\Dev;
use App\Core\Controller;
use App\Core\View;
/**
 *  Paths
 */
class Phpinfo extends Controller
{
	
	public function __construct()
	{
		parent::__construct();
	}

	public function IndexAction()
	{
			View::render('d0', 'phpinfo');
	}

    protected function before(){}

    protected function after(){}

} //END CLASS