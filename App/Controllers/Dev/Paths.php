<?php
namespace App\Controllers\Dev;
use App\Core\Controller;
use App\Core\View;
/**
 *  Paths
 */
class Paths extends Controller
{
	
	public function __construct()
	{
		parent::__construct();
	}

	public function IndexAction()
	{
			View::render('d0', 'paths');
	}

    protected function before(){}

    protected function after(){}

} //END CLASS