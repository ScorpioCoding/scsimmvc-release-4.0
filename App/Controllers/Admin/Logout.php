<?php
namespace App\Controllers\Admin;
use App\Core\Controller;
use App\Core\View;
use App\Models\mLogout;
/**
 *  Logout
 */
class Logout extends Controller
{
    public function __construct()
	{
		parent::__construct();
	}

	protected function before()
	{

	}
    public function IndexAction()
	{
			View::render('b1', 'logout');
	}

	public function runAction()
	{	
		mLogout::run();
	}
    

    protected function after(){}


} //END CLASS